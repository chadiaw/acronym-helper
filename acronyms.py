from collections import defaultdict
import pickle


class AcronymHelper:

    def __init__(self, file):
        self.file_path = file
        self.data = self.load_data()
        print("Currently {} acronyms in the data set".format(len(self.data)))

    def load_data(self):
        try:
            pkl_file = open(self.file_path, 'rb')
            data = pickle.load(pkl_file)
            pkl_file.close()
        except IOError:
            print("No such file found. Initializing new data set.")
            data = defaultdict(set)
        return data

    def save_data(self):
        pkl_file = open(self.file_path, 'wb')
        pickle.dump(self.data, pkl_file)
        pkl_file.close()

    def add_entry(self, key, value):
        self.data[key].add(value)
        print("Entry added.")
        self.save_data()

    def display_entry(self, key):
        if self.data.get(key) is None:
            print("No entry for '{}'.".format(key))
            alternatives = list()
            for acronym in self.data.keys():
                if key.lower() == acronym.lower():
                    alternatives.append(acronym)
            if len(alternatives) > 0:
                print("Following similar acronyms were found: {}.".format(", ".join(alternatives)))
            return

        for value in self.data[key]:
            print("{}: {}".format(key, value))
        print("--")

    def display_all(self):
        if len(self.data) == 0:
            print("Data set is currently empty.")
        for key in self.data.keys():
            self.display_entry(key)

    def remove_entries(self, key):
        try:
            self.data.pop(key)
            print("All entries for '{}' removed.".format(key))
        except KeyError:
            print("No entries for '{}'.".format(key))

    def parse_command(self, command):
        args = command.split(" ")
        if args[0] == "add":
            self.add_entry(args[1], " ".join(args[2:]))
        elif args[0] == "del" or args[0] == "delete" or args[0] == "remove":
            self.remove_entries(args[1])
        elif args[0] == "all":
            self.display_all()
        elif args[0] == "exit":
            exit()
        else:
            print("Unknown command. Usage: \n"
                  "\tadd KEY value\n \t Adds the value for the acronym KEY "
                  "\tall \t Displays all entries"
                  "\t remove KEY \t Removes all entries for the KEY acronym")


if __name__ == '__main__':
    file_path = input("Enter db name (will be used to load and store data) : ")
    acronym_mgr = AcronymHelper(file_path)
    while True:
        command = input("> ")
        if command == "exit" or command == "quit":
            exit()
        elif not command.startswith("#"):
            acronym_mgr.display_entry(command)
        else:
            acronym_mgr.parse_command(command[1:])
